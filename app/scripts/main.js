$(document).ready(function () {

    /** слайдер в хедере главной страницы **/
    (function () {
        var owl = $(".slider-main").owlCarousel({
            margin: 0,
            loop: true,
            nav: false,
            dotsContainer: '.nav_slider-main',
            items: 1,
            mouseDrag: false,
            autoplay: true,
            animateOut: 'fadeOut'
        });
        $('.nav_slider-main > li').click(function () {
            owl.trigger('to.owl.carousel', [$(this).index(), 300]);
        });
    }( jQuery ));

    /** календарь в хедере на главной **/

    // слайдер застройщиков
    (function () {
        $(".slider-builders").owlCarousel({
            margin: 0,
            loop: true,
            nav: true,
            items: 6,
            autoplay: true,
            autoplayTimeout: 4000,
            autoplayHoverPause: false
        });
    }( jQuery ));
    // слайдер отзывов
    (function () {
        $(".slider-reviews").owlCarousel({
            margin:0,
            padding:20,
            loop:false,
            nav:true,
            navText:["Предыдущий", "Следующий"],
            items:3,
            autoplay:true,
            autoplayTimeout:4000,
            autoplayHoverPause:false
        });
    }( jQuery ));
    // слайдер сертификатов
    (function () {
        $(".slider-certificate").owlCarousel({
            margin:0,
            padding:20,
            loop:false,
            nav:true,
            navText:["Предыдущий", "Следующий"],
            items:6,
            autoplay:true,
            autoplayTimeout:4000,
            autoplayHoverPause:false
        });
    }( jQuery ));
    // слайдер изображений жк
    (function () {
        $(".slider-object-photo").owlCarousel({
            loop: true,
            nav:true,
            navText:["Предыдущий", "Следующий"],
            items: 4,
            margin:5,
            autoplay: false
        });
    }( jQuery ));
    // реформат свг
    $(".js_reform_svg").each(function() {
        var $img = jQuery(this);
        var attributes = $img.prop("attributes");
        var imgURL = $img.attr("src");
        $.get(imgURL, function(data) {
            var $svg = $(data).find('svg');
            $svg = $svg.removeAttr('xmlns:a');
            $.each(attributes, function() {
                $svg.attr(this.name, this.value)
            });
            $img.replaceWith($svg)
        })
    });
    //инициализация селекта без поисковика
    $('.js_select').select2({
        minimumResultsForSearch: Infinity
    });
    //инициализация селекта с поисковиком
    $('.js_select_search').select2();
});